"""__init__.py
Usage:
    __init__.py <token> <target>
"""
import sys
import discord
from docopt import docopt
from pathlib import Path
from delete_message import DeleteMessage


if __name__ == '__main__':
    args = docopt(__doc__)
    token_file = args["<token>"]
    target_user = args["<target>"]

    path = Path(__file__).parent / Path(token_file)
    if not path.exists():
        sys.exit(1)
    with path.open("r", encoding="utf-8") as f:
        token = f.read().replace("\n", "")
    client = DeleteMessage(target_user)
    try:
        client.run(token, bot=False)
    except discord.errors.LoginFailure:
        print(f"Login failure: improper token in '{path.resolve()}'")
