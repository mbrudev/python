import discord
import datetime
import asyncio

DATE_FROM = datetime.datetime(2015, 5, 13)
DATE_TO = datetime.datetime.utcnow().replace(microsecond=0)


class DeleteMessage(discord.Client):

    def __init__(self, user, date_from=DATE_FROM, date_to=DATE_TO, limit=None, *, loop=None, **options):
        super().__init__(loop=loop, **options)
        self.name, self.number = user.split("#")
        self.limit = limit
        self.date_to = date_to
        self.date_from = date_from
        self.msg_counter = 0

    async def on_ready(self):
        try:
            print(f"discord client logged in as {self.user}")
            dm_channels = self.__list_channels()
            dm_recipient = self.__find_recipient(dm_channels)
            async for msg in dm_recipient.history(limit=self.limit, after=self.date_from, before=self.date_to):
                if msg.author == self.user and msg.type == discord.MessageType.default:
                    await msg.delete()
                    self.msg_counter += 1
                    if self.msg_counter % 100 == 0:
                        print("\n")
                    print("*", end="", flush=True)
                    await asyncio.sleep(0.1)
            print(f"\n{self.msg_counter} messages deleted.")
        except asyncio.CancelledError:
            print(f"\nDelete canceled after {self.msg_counter} messages.")
        except Exception as e:
            print(f"\nError: {str(e)}")
        finally:
            await self.close()

    def __list_channels(self):
        return (c for c in self.private_channels if isinstance(c, discord.DMChannel))

    def __find_recipient(self, channels):
        try:
            dm_channel = list(c for c in channels
                              if c.recipient.name == self.name and c.recipient.discriminator == self.number)[0]
        except IndexError:
            raise Exception(f"user not found: '{self.name}#{self.number}'")
        return dm_channel
